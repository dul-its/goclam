FROM golang:1-bullseye

SHELL ["/bin/bash", "-c"]

ENV TZ=US/Eastern

WORKDIR /go/src/goclam

COPY . .

VOLUME /data

RUN set -eux; \
	go mod init gitlab.oit.duke.edu/dul-its/goclam; \
	go mod tidy; \
	go install; \
	useradd -r -u 1001 -g 0 goclam

USER goclam

CMD ["goclam"]
