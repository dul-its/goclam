# goclam: Golang HTTP middleware to ClamAV daemon (clamd)

## Build

    $ make

## Test

    $ make clean test

## Interactive

	$ mkdir -p data
	$ cp file data/
    $ docker-compose up --build
	$ curl -G -d path=/data/file http://localhost:8888/scan
