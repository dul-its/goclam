#!/bin/bash

container="gclam-test-${CI_JOB_ID:-1}"

docker-compose run --name $container -d goclam

code=1
SECONDS=0
while [[ $SECONDS -lt 60 ]]; do
    echo -n "---> Checking status ... "
    status=$(docker inspect -f '{{.State.Health.Status}}' ${container})
    echo "${status}"

    if [[ "${status}" == "healthy" ]]; then
	code=0
	break
    fi

    sleep 5
done

docker-compose down

exit $code
