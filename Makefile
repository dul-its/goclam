SHELL = /bin/bash

build_tag ?= goclam

.PHONY: build
build:
	DOCKER_BUILDKIT=1 docker build -t $(build_tag) -f ./Dockerfile ./src

.PHONY: clean
clean:
	echo 'no-op'

.PHONY: test
test:
