package main

import (
	"bytes"
	"encoding/binary"
	"flag"
	"fmt"
	"io"
	"io/ioutil"
	"log"
	"net"
	"net/http"
	"os"
)

const MaxFileSize = 26214400 // (25Mb) default max for ClamAV

var (
	clamdTcpAddr string
	goclamAddr string
)

func init() {
	var (
		present bool
		defaultClamdTcpHost string
		defaultClamdTcpSock string
		defaultGoclamPort string
		defaultGoclamBind string
	)

	defaultClamdTcpHost, present = os.LookupEnv("CLAMD_TCP_HOST")
	if !present {
		defaultClamdTcpHost = "localhost"
	}
	var clamdTcpHost = flag.String(
		"clamd-tcp-host",
		defaultClamdTcpHost,
		"clamd TCP host address",
	)

	defaultClamdTcpSock, present = os.LookupEnv("CLAMD_TCP_SOCK")
	if !present {
		defaultClamdTcpSock = "3310"
	}
	var clamdTcpSock = flag.String(
		"clamd-tcp-sock",
		defaultClamdTcpSock,
		"clamd TCP socket",
	)

	defaultGoclamPort, present = os.LookupEnv("GOCLAM_PORT")
	if !present {
		defaultGoclamPort = "8888"
	}
	var goclamPort = flag.String(
		"p",
		defaultGoclamPort,
		"Port on which goclam listens",
	)

	defaultGoclamBind, present = os.LookupEnv("GOCLAM_BIND")
	if !present {
		defaultGoclamBind = "0.0.0.0"
	}
	var goclamBind = flag.String(
		"b",
		defaultGoclamBind,
		"Interface to which goclam binds",
	)

	flag.Parse()

	clamdTcpAddr = fmt.Sprintf("%s:%s", *clamdTcpHost, *clamdTcpSock)
	log.Printf("Clamd expected at %s", clamdTcpAddr)

	goclamAddr = fmt.Sprintf("%s:%s", *goclamBind, *goclamPort)
	log.Printf("GoClam bound to %s", goclamAddr)
}

func clamdSendAndReceive(cmd string) (string, error) {
	log.Printf("Request: %s", cmd)

	// connect to clamd
	conn, err := net.Dial("tcp", clamdTcpAddr)
	defer conn.Close()
	if err != nil {
		return "Unable to connect to Clamd", err
	}

	// send the command
	fmt.Fprintf(conn, "n%s\n", cmd)

	// read the response
	resp, err := ioutil.ReadAll(conn)

	// return the response text
	result := fmt.Sprintf("%s", resp)

	return result, err
}

func clamdRequest(w http.ResponseWriter, cmd string) {
	result, err := clamdSendAndReceive(cmd)

	if err != nil {
		http.Error(w, err.Error(), 500)
	} else {
		fmt.Fprint(w, result)
	}
}

func pingHandler(w http.ResponseWriter, _ *http.Request) {
	clamdRequest(w, "PING")
}

func scanHandler(w http.ResponseWriter, r *http.Request) {
	path := r.URL.Query().Get("path")
	cmd := fmt.Sprintf("SCAN %s", path)
	clamdRequest(w, cmd)
}

func streamHandler(w http.ResponseWriter, r *http.Request) {
	// get the file
	file, file_header, err := r.FormFile("file")
	defer file.Close()
	if err != nil {
		http.Error(w, err.Error(), 500)
		return
	}

	// check file size
	log.Printf("File upload: %s (%d bytes)", file_header.Filename, file_header.Size)
	if file_header.Size > MaxFileSize {
		// too big - abort
		err = fmt.Errorf("Cannot scan files larger than %d", MaxFileSize)
		http.Error(w, err.Error(), 403)
		return
	}

	// get a clamd connection
	conn, err := net.Dial("tcp", clamdTcpAddr)
	defer conn.Close()
	if err != nil { //connection error
		http.Error(w, err.Error(), 503)
		return
	}

	// send stream command
	log.Print("Sending INSTREAM command ...")
	_, err = fmt.Fprint(conn, "zINSTREAM\x00")
	if err != nil { // communication error
		http.Error(w, err.Error(), 500)
		return
	}

	var buf bytes.Buffer
	var header [4]byte

	log.Print("Streaming file ...")
	for {
		n, rerr := buf.ReadFrom(file)

		log.Printf("Read %d bytes from file", n)
		binary.BigEndian.PutUint32(header[0:4], uint32(n))

		o, werr := conn.Write(append(header[0:4], buf.Bytes()...))
		log.Printf("Wrote %d bytes to Clamd", o)
		if werr != nil {
			// write error - abort
			http.Error(w, werr.Error(), 500)
			return
		}

		if rerr != nil && rerr != io.EOF {
			// there was some kind of read error earlier - abort
			http.Error(w, rerr.Error(), 500)
			return
		}

		if n == 0 {
			log.Print("Done streaming")
			break
		} else {
			buf.Reset()
		}
	}

	// get response from clamd
	resp, err := ioutil.ReadAll(conn)
	if err != nil {
		http.Error(w, err.Error(), 500)
	}

	// return the response text
	fmt.Fprintf(w, "%s", resp)
}

func versionHandler(w http.ResponseWriter, _ *http.Request) {
	clamdRequest(w, "VERSION")
}

func statsHandler(w http.ResponseWriter, _ *http.Request) {
	clamdRequest(w, "STATS")
}

func main() {
	// Not Implemented:
	// RELOAD
	// SHUTDOWN
	// RAWSCAN file/directory
	// CONTSCAN file/directory
	// MULTISCAN file/directory
	// ALLMATCHSCAN file/directory

	http.Handle("/", http.NotFoundHandler())
	http.HandleFunc("/ping", pingHandler)
	http.HandleFunc("/scan", scanHandler)
	http.HandleFunc("/stats", statsHandler)
	http.HandleFunc("/stream", streamHandler)
	http.HandleFunc("/version", versionHandler)

	log.Fatal(http.ListenAndServe(goclamAddr, nil))
}
